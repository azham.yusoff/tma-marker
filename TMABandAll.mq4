//------------------------------------------------------------------
//
//------------------------------------------------------------------
#property copyright "Ambalar"
#property link      "www.forex-tsd.com"

#property indicator_chart_window
#property indicator_buffers 22


// fast lane 
#property indicator_color1 clrDodgerBlue
#property indicator_width1 1

// fast upper 2.5
#property indicator_color2 clrDodgerBlue
#property indicator_width2 2
#property indicator_style2 STYLE_DOT
#property indicator_label2 "Fast lane 2.5 (Upper)"
// fast lower 2.5
#property indicator_color3 clrDodgerBlue
#property indicator_width3 2
#property indicator_style3 STYLE_DOT
#property indicator_label3 "Fast lane 2.5 (Lower)"


// slow lane 
#property indicator_color4 clrMediumSeaGreen
#property indicator_width4 3

// slow upper 2.5
#property indicator_color5 clrRed
#property indicator_width5 2
#property indicator_style5 STYLE_DOT
#property indicator_label5 "Slow lane 2.5 (Upper)"
// slow lower 2.5
#property indicator_color6 clrRed
#property indicator_width6 2
#property indicator_style6 STYLE_DOT
#property indicator_label6 "Slow lane 2.5 (Lower)"

// slow lower 5.0
#property indicator_color7 clrOrange
#property indicator_width7 2
#property indicator_style7 STYLE_DOT
#property indicator_label7 "Slow lane 5.0 (Upper)"
// slow lower 5.0
#property indicator_color8 clrOrange
#property indicator_width8 2
#property indicator_style8 STYLE_DOT
#property indicator_label8 "Slow lane 5.0 (Lower)"

// slow lower 5.5
#property indicator_color9 clrGold
#property indicator_width9 3
#property indicator_style9 STYLE_DOT
#property indicator_label9 "Slow lane 5.5 (Upper)"
// slow lower 5.5
#property indicator_color10 clrGold
#property indicator_width10 3
#property indicator_style10 STYLE_DOT
#property indicator_label10 "Slow lane 5.5 (Lower)"

// slow lower 7.5
#property indicator_color11 clrDeepPink
#property indicator_width11 1
#property indicator_label11 "Slow lane 7.5 (Upper)"
// slow lower 7.5
#property indicator_color12 clrDeepPink
#property indicator_width12 1
#property indicator_label12 "Slow lane 7.5 (Lower)"

// slow lower 10.0
#property indicator_color13 clrDeepPink
#property indicator_width13 4
#property indicator_style13 STYLE_DOT
#property indicator_label13 "Slow lane 10.0 (Upper)"
// slow lower 10.0
#property indicator_color14 clrDeepPink
#property indicator_width14 4
#property indicator_style14 STYLE_DOT
#property indicator_label14 "Slow lane 10.0 (Lower)"

// marker sell fast
#property indicator_type15 DRAW_ARROW
#property indicator_width15 2
#property indicator_label15 "Fast Correction (Sell)"
#property indicator_color15 clrMagenta

// marker buy fast
#property indicator_type16 DRAW_ARROW
#property indicator_width16 2
#property indicator_label16 "Fast Correction (Buy)"
#property indicator_color16 clrBlue


// marker sell slow golden - sell 16
#property indicator_type17 DRAW_ARROW
#property indicator_width17 2
#property indicator_label17 "Fast Correction (Sell)"
#property indicator_color17 clrGold

// marker buy slow golden - buy 17
#property indicator_type18 DRAW_ARROW
#property indicator_width18 2
#property indicator_label18 "Fast Correction (Buy)"
#property indicator_color18 clrGold


// marker sell slow extreme - sell 18
#property indicator_type19 DRAW_ARROW
#property indicator_width19 2
#property indicator_label19 "Fast Correction (Sell)"
#property indicator_color19 clrDeepPink

// marker buy slow extreme - buy 19
#property indicator_type20 DRAW_ARROW
#property indicator_width20 2
#property indicator_label20 "Fast Correction (Buy)"
#property indicator_color20 clrDeepPink


// sl
#property indicator_color21 clrDeepPink
#property indicator_width21 1
#property indicator_style21 STYLE_DOT
#property indicator_label21 "Slow lane 15.0 (Upper)"
// sl
#property indicator_color22 clrDeepPink
#property indicator_width22 1
#property indicator_style22 STYLE_DOT
#property indicator_label22 "Slow lane 15.0 (Lower)"
//
//
//
//
//

string _currency_;
int    HalfLength[] = {21, 61};
int    Price      = PRICE_CLOSE;
double ATRMultiplier[]   = {2.5, 5.0, 5.5, 7.5, 10, 15};
int    ATRPeriod       = 110;

int _digit_, _digit_multiplier_;
double _min_lot_,_pip_value_,_point_;


double buffer1[], buffer2[];
double bandUp25f[];
double bandDn25f[];

double bandUp25s[];
double bandDn25s[];
double bandUp50s[];
double bandDn50s[];
double bandUp55s[];
double bandDn55s[];
double bandUp75s[];
double bandDn75s[];
double bandUp100s[];
double bandDn100s[];

double slope[], slopeSlow[], sell25f[], buy25f[], sell50s[], buy50s[], sell100s[], buy100s[];

double slBuy150[], slSell150[];
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//

int init()
{
   //Comment("This is new... " + EMPTY_VALUE);
   _currency_ = AccountInfoString(ACCOUNT_CURRENCY);
   _digit_ = Digits;
   
   _point_= Point;
   _point_*=((_digit_==3) || (_digit_==5)) ? 10 : 1;
   
   _digit_multiplier_ = (_digit_==2?100:(_digit_==3?100:(_digit_==4?1000:(_digit_==5?10000:(_digit_==6?100000:(_digit_==7?1000000:1))))));
   //_digit_multiplier_ = _point_;
   
   
   _pip_value_ =(((MarketInfo(Symbol(),MODE_TICKVALUE)*_point_)/MarketInfo(Symbol(),MODE_TICKSIZE))*MarketInfo(Symbol(),MODE_MINLOT));
   
   //_min_lot_ = MarketInfo(Symbol(),MODE_MINLOT);

   HalfLength[0]=MathMax(HalfLength[0],1);
   IndicatorBuffers(23);
   SetIndexBuffer(0,buffer1);  SetIndexDrawBegin(0,HalfLength[0]);
   SetIndexBuffer(1,bandUp25f);   SetIndexDrawBegin(1,HalfLength[0]);
   SetIndexBuffer(2,bandDn25f);   SetIndexDrawBegin(2,HalfLength[0]);
   
   SetIndexBuffer(3,buffer2);  SetIndexDrawBegin(3,HalfLength[1]);
   SetIndexBuffer(4,bandUp25s);   SetIndexDrawBegin(4,HalfLength[1]);
   SetIndexBuffer(5,bandDn25s);   SetIndexDrawBegin(5,HalfLength[1]);
   SetIndexBuffer(6,bandUp50s);   SetIndexDrawBegin(6,HalfLength[1]);
   SetIndexBuffer(7,bandDn50s);   SetIndexDrawBegin(7,HalfLength[1]);
   SetIndexBuffer(8,bandUp55s);   SetIndexDrawBegin(8,HalfLength[1]);
   SetIndexBuffer(9,bandDn55s);   SetIndexDrawBegin(9,HalfLength[1]);
   SetIndexBuffer(10,bandUp75s);   SetIndexDrawBegin(10,HalfLength[1]);
   SetIndexBuffer(11,bandDn75s);   SetIndexDrawBegin(11,HalfLength[1]);
   SetIndexBuffer(12,bandUp100s);   SetIndexDrawBegin(12,HalfLength[1]);
   SetIndexBuffer(13,bandDn100s);   SetIndexDrawBegin(13,HalfLength[1]);
   
   
   SetIndexBuffer(14,sell25f);   SetIndexDrawBegin(14,HalfLength[0]);
   SetIndexStyle(14,DRAW_ARROW);
   SetIndexArrow(14,164);
   
   SetIndexBuffer(15,buy25f);   SetIndexDrawBegin(15,HalfLength[0]);
   SetIndexStyle(15,DRAW_ARROW);
   SetIndexArrow(15,164);
   
   SetIndexBuffer(16,sell50s);   SetIndexDrawBegin(16,HalfLength[1]);
   SetIndexStyle(16,DRAW_ARROW);
   SetIndexArrow(16,174);
   
   SetIndexBuffer(17,buy50s);   SetIndexDrawBegin(17,HalfLength[1]);
   SetIndexStyle(17,DRAW_ARROW);
   SetIndexArrow(17,174);
   
   SetIndexBuffer(18,sell100s);   SetIndexDrawBegin(18,HalfLength[1]);
   SetIndexStyle(18,DRAW_ARROW);
   SetIndexArrow(18,174);
   
   SetIndexBuffer(19,buy100s);   SetIndexDrawBegin(19,HalfLength[1]);
   SetIndexStyle(19,DRAW_ARROW);
   SetIndexArrow(19,174);
   
   SetIndexBuffer(20,slSell150); SetIndexDrawBegin(20,HalfLength[1]);
   SetIndexBuffer(21,slBuy150); SetIndexDrawBegin(21,HalfLength[1]);
   
   SetIndexBuffer(22,slope);
   SetIndexBuffer(23,slopeSlow);
   
   
   
   
   
   
   ObjectDelete(0, "upper100");
   ObjectDelete(0, "lower100");
   ObjectDelete(0, "upper50");
   ObjectDelete(0, "lower50");
   ObjectDelete(0, "upper25");
   ObjectDelete(0, "lower25");
   
   ObjectDelete(0, "buffer1");
   
   //if(MarketInfo(Symbol(),MODE_MARGINCALCMODE)==0){
   
      CreateLabel(0,"upper100",0,0,"upper100",8,clrDeepPink);
      CreateLabel(0,"lower100",0,0,"lower100",8,clrDeepPink);
      
      CreateLabel(0,"upper50",0,0,"upper50",8,clrOrange);
      CreateLabel(0,"lower50",0,0,"lower50",8,clrOrange);
      
      CreateLabel(0,"upper25",0,0,"upper25",8,clrDodgerBlue);
      CreateLabel(0,"lower25",0,0,"lower25",8,clrDodgerBlue);
      
      CreateLabel(0,"buffer1",0,0,"buffer1",8,clrMediumSeaGreen);
   //}
   
   return(0);
}

int deinit() { 
   
   ObjectDelete(0, "upper100");
   ObjectDelete(0, "lower100");
   ObjectDelete(0, "upper50");
   ObjectDelete(0, "lower50");
   ObjectDelete(0, "upper25");
   ObjectDelete(0, "lower25");
   
   ObjectDelete(0, "buffer1");

   return(0); 
  }




//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//
//
//
//
//

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   int counted_bars=IndicatorCounted();
   int i, limit;
   //i,j,k,
   if(counted_bars<0) return(-1);
   if(counted_bars>0) counted_bars--;
           limit=MathMin(Bars-counted_bars+HalfLength[0],Bars-1);

   
   
   for (i=limit;i>=0;i--)
   {
     fastOne(i);
     slowOne(i);
     
     //bandUp100s[i] > 0 && bandDn100s[i] > 0 && bandUp100s[i] > bandDn100s[i]
     if(bandUp100s[i] > 0 && bandDn100s[i] > 0 && bandUp100s[i] > bandDn100s[i]){
        sell25f[i] = 0;
        sell25f[i] =  high[i] >= bandUp25f[i] ? high[i] : 0;
        sell25f[i+1] = (sell25f[i] > 0 && sell25f[i+1] > 0) && (sell25f[i] > sell25f[i+1]) ? 0 : sell25f[i+1];
        sell25f[i] = (sell25f[i] > 0 && sell25f[i+1] > 0) && (sell25f[i+1] > sell25f[i]) ? 0 : sell25f[i];
        
        buy25f[i] = 0;
        buy25f[i] =  low[i] <= bandDn25f[i] ? low[i] : 0;
        buy25f[i+1] = (buy25f[i] > 0 && buy25f[i+1] > 0) && (buy25f[i] < buy25f[i+1]) ? 0 : buy25f[i+1];
        buy25f[i] = (buy25f[i] > 0 && buy25f[i+1] > 0) && (buy25f[i+1] < buy25f[i]) ? 0 : buy25f[i];
        
        sell50s[i] = 0;
        sell50s[i] =  high[i] >= bandUp50s[i] ? high[i] : 0;
        sell50s[i+1] = (sell50s[i] > 0 && sell50s[i+1] > 0) && (sell50s[i] > sell50s[i+1]) ? 0 : sell50s[i+1];
        sell50s[i] = (sell50s[i] > 0 && sell50s[i+1] > 0) && (sell50s[i+1] > sell50s[i]) ? 0 : sell50s[i];
        
        buy50s[i] = 0;
        buy50s[i] =  low[i] <= bandDn50s[i] ? low[i] : 0;
        buy50s[i+1] = (buy50s[i] > 0 && buy50s[i+1] > 0) && (buy50s[i] < buy50s[i+1]) ? 0 : buy50s[i+1];
        buy50s[i] = (buy50s[i] > 0 && buy50s[i+1] > 0) && (buy50s[i+1] < buy50s[i]) ? 0 : buy50s[i];
        
        sell100s[i] = 0;
        sell100s[i] =  high[i] >= bandUp100s[i] ? high[i] : 0;
        sell100s[i+1] = (sell100s[i] > 0 && sell100s[i+1] > 0) && (sell100s[i] > sell100s[i+1]) ? 0 : sell100s[i+1];
        sell100s[i] = (sell100s[i] > 0 && sell100s[i+1] > 0) && (sell100s[i+1] > sell100s[i]) ? 0 : sell100s[i];
        
        buy100s[i] = 0;
        buy100s[i] =  low[i] <= bandDn100s[i] ? low[i] : 0;
        buy100s[i+1] = (buy100s[i] > 0 && buy100s[i+1] > 0) && (buy100s[i] < buy100s[i+1]) ? 0 : buy100s[i+1];
        buy100s[i] = (buy100s[i] > 0 && buy100s[i+1] > 0) && (buy100s[i+1] < buy100s[i]) ? 0 : buy100s[i];
     }
   }
   
   long pip_upper100 = MathAbs(bandUp100s[0]-close[0])*_digit_multiplier_;
   long pip_lower100 = MathAbs(close[0]-bandDn100s[0])*_digit_multiplier_;
   
   long pip_upper50 = MathAbs(bandUp50s[0]-close[0])*_digit_multiplier_;
   long pip_lower50 = MathAbs(close[0]-bandDn50s[0])*_digit_multiplier_;
   
   long pip_upper25 = MathAbs(bandUp25f[0]-close[0])*_digit_multiplier_;
   long pip_lower25 = MathAbs(close[0]-bandDn25f[0])*_digit_multiplier_;
   
   long pip_buffer2 = MathAbs(close[0]-buffer2[0])*_digit_multiplier_;
   
   double v_u100 = _pip_value_*pip_upper100;
   double v_l100 = _pip_value_*pip_lower100;
   
   double v_u50 = _pip_value_*pip_upper50;
   double v_l50 = _pip_value_*pip_lower50;
   
   double v_u25 = _pip_value_*pip_upper25;
   double v_l25 = _pip_value_*pip_lower25;
   
   double v_buff = _pip_value_*pip_buffer2;
   
   
   TextMove(0, "upper100", TimeCurrent(), bandUp100s[0], pip_upper100 + " pips (" + _currency_ + DoubleToStr(v_u100,2) + ")");
   TextMove(0, "lower100", TimeCurrent(), bandDn100s[0], pip_lower100 + " pips (" + _currency_ + DoubleToStr(v_l100,2) + ")");
   TextMove(0, "upper50", TimeCurrent(), bandUp50s[0], pip_upper50 + " pips (" + _currency_ + DoubleToStr(v_u50,2) + ")");
   TextMove(0, "lower50", TimeCurrent(), bandDn50s[0], pip_lower50 + " pips (" + _currency_ + DoubleToStr(v_l50,2) + ")");
   TextMove(0, "upper25", TimeCurrent(), bandUp25f[0], pip_upper25 + " pips (" + _currency_ + DoubleToStr(v_u25,2) + ")");
   TextMove(0, "lower25", TimeCurrent(), bandDn25f[0], pip_lower25 + " pips (" + _currency_ + DoubleToStr(v_l25,2) + ")");
   TextMove(0, "buffer1", TimeCurrent(), buffer2[0], pip_buffer2 + " pips (" + _currency_ + DoubleToStr(v_buff,2) + ")");
   
   //Print("bandUp100s[0]: ",bandUp100s[0]," | time[0] ", time[0]);
   //manageAlerts();
   return(0);
}


void fastOne(int i){
   int j,k;
    double sum  = (HalfLength[0]+1)*iMA(NULL,0,1,0,MODE_LWMA,Price,i);
      double sumw = (HalfLength[0]+1);
      for(j=1, k=HalfLength[0]; j<=HalfLength[0]; j++, k--)
      {
         sum  += k*iMA(NULL,0,1,0,MODE_LWMA,Price,i+j);
         sumw += k;
         if (j<=i)
         {
            sum  += k*iMA(NULL,0,1,0,MODE_LWMA,Price,i-j);
            sumw += k;
         }
      }
      double range = iATR(NULL,0,ATRPeriod,i+10);
      buffer1[i]  = sum/sumw;  
      bandUp25f[i]   = buffer1[i]+(range*ATRMultiplier[0]);
      bandDn25f[i]   = buffer1[i]-(range*ATRMultiplier[0]);
      //bandUp50f[i]   = buffer1[i]+(range*ATRMultiplier[1]);
      //bandDn50f[i]   = buffer1[i]-(range*ATRMultiplier[1]);
      //buffer2a[i] = EMPTY_VALUE;
      //buffer2b[i] = EMPTY_VALUE;
      slope[i]    = slope[i+1];
         if (buffer1[i]>buffer1[i+1]) slope[i] =  1;
         if (buffer1[i]<buffer1[i+1]) slope[i] = -1;
         //if (slope[i]==-1) PlotPoint(i,buffer2a,buffer2b,buffer1);
}


void slowOne(int i){
   int j,k;
    double sum  = (HalfLength[1]+1)*iMA(NULL,0,1,0,MODE_LWMA,Price,i);
      double sumw = (HalfLength[1]+1);
      for(j=1, k=HalfLength[1]; j<=HalfLength[1]; j++, k--)
      {
         sum  += k*iMA(NULL,0,1,0,MODE_LWMA,Price,i+j);
         sumw += k;
         if (j<=i)
         {
            sum  += k*iMA(NULL,0,1,0,MODE_LWMA,Price,i-j);
            sumw += k;
         }
      }
      double range = iATR(NULL,0,ATRPeriod,i+10);
      buffer2[i]  = sum/sumw;  
      bandUp25s[i]   = buffer2[i]+(range*ATRMultiplier[0]);
      bandDn25s[i]   = buffer2[i]-(range*ATRMultiplier[0]);
      bandUp50s[i]   = buffer2[i]+(range*ATRMultiplier[1]);
      bandDn50s[i]   = buffer2[i]-(range*ATRMultiplier[1]);
      bandUp55s[i]   = buffer2[i]+(range*ATRMultiplier[2]);
      bandDn55s[i]   = buffer2[i]-(range*ATRMultiplier[2]);
      bandUp75s[i]   = buffer2[i]+(range*ATRMultiplier[3]);
      bandDn75s[i]   = buffer2[i]-(range*ATRMultiplier[3]);
      bandUp100s[i]   = buffer2[i]+(range*ATRMultiplier[4]);
      bandDn100s[i]   = buffer2[i]-(range*ATRMultiplier[4]);
      
      
      slSell150[i]   = buffer2[i]+(range*ATRMultiplier[5]);
      slBuy150[i]   = buffer2[i]-(range*ATRMultiplier[5]);
      //buffer2a[i] = EMPTY_VALUE;
      //buffer2b[i] = EMPTY_VALUE;
      slopeSlow[i]    = slopeSlow[i+1];
         if (buffer2[i]>buffer2[i+1]) slopeSlow[i] =  1;
         if (buffer2[i]<buffer2[i+1]) slopeSlow[i] = -1;
         //if (slope[i]==-1) PlotPoint(i,buffer2a,buffer2b,buffer1);
}



//+-------------------------------------------------------------------
//|                                                                  
//+-------------------------------------------------------------------
//
//
//
//
//

//void manageAlerts()
//{
//   if (alertsOn)
//   {
//      if (slope[0] == 1) doAlert("up");
//      if (slope[0] ==-1) doAlert("down");
//   }
//}

//
//
//
//
//

//void doAlert(string doWhat)
//{
//   static string   previousAlert="nothing";
//   string message;
//   
//   if (previousAlert != doWhat ) {
//       previousAlert  = doWhat;
//
//       //
//       //
//       //
//       //
//       //
//
//       message =  Symbol()+" at "+TimeToStr(TimeLocal(),TIME_SECONDS)+" TMA slope is currently "+doWhat;
//          if (alertsMessage) Alert(message);
//          if (alertsEmail)   SendMail(StringConcatenate(Symbol(),"TMA centered & bands "),message);
//          if (alertsSound)   PlaySound("alert2.wav");
//   }
//}

//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
//
//
//
//
//

void CleanPoint(int i,double& first[],double& second[])
{
   if ((second[i]  != EMPTY_VALUE) && (second[i+1] != EMPTY_VALUE))
        second[i+1] = EMPTY_VALUE;
   else
      if ((first[i] != EMPTY_VALUE) && (first[i+1] != EMPTY_VALUE) && (first[i+2] == EMPTY_VALUE))
          first[i+1] = EMPTY_VALUE;
}

//
//
//
//
//

void PlotPoint(int i,double& first[],double& second[],double& from[])
{
   if (first[i+1] == EMPTY_VALUE)
      {
         if (first[i+2] == EMPTY_VALUE) {
                first[i]   = from[i];
                first[i+1] = from[i+1];
                second[i]  = EMPTY_VALUE;
            }
         else {
                second[i]   =  from[i];
                second[i+1] =  from[i+1];
                first[i]    = EMPTY_VALUE;
            }
      }
   else
      {
         first[i]  = from[i];
         second[i] = EMPTY_VALUE;
      }
}


bool CreateLabel (long chart_ID, string name, datetime x, int y , string text, int fontSize, color clr)
  {
   ResetLastError();
   if (!ObjectCreate (0,name, OBJ_TEXT, 0, x, y))
     {
      Print (__FUNCTION__, ": Unable to create the text! Error code = ", GetLastError());
      return(false);
     }
     
   ObjectSetString  (chart_ID, name, OBJPROP_TEXT, text);
   ObjectSetInteger (chart_ID, name, OBJPROP_CORNER, 0);
   ObjectSetInteger (chart_ID, name, OBJPROP_FONTSIZE, fontSize);
   ObjectSetInteger (chart_ID, name, OBJPROP_COLOR, clr);
   ObjectSetInteger (chart_ID, name, OBJPROP_BACK, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_STATE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTABLE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTED, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_HIDDEN, true);
   ObjectSetInteger (chart_ID, name, OBJPROP_ZORDER,0);
   ObjectSetString  (chart_ID, name, OBJPROP_FONT, "Arial");
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,ANCHOR_LEFT_UPPER);
   
   return(true);
  }
  
 bool TextMove(const long   chart_ID=0,  // chart's ID 
              const string name="Text", // object name 
              datetime     time=0,      // anchor point time coordinate 
              double       price=0, // anchor point price coordinate
              string       pips = "")      
  { 
//--- if point position is not set, move it to the current bar having Bid price 
   if(!time) 
      time=TimeCurrent(); 
   if(!price) 
      price=SymbolInfoDouble(Symbol(),SYMBOL_BID); 
//--- reset the error value 
   ResetLastError(); 
//--- move the anchor point 
   if(!ObjectMove(chart_ID,name,0,time,price)) 
     { 
      Print(__FUNCTION__, 
            ": failed to move the anchor point! Error code = ",GetLastError()); 
      return(false); 
     } 
     ObjectSetString  (0, name, OBJPROP_TEXT, pips);
//--- successful execution 
   return(true); 
  } 