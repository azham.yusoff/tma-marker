//+------------------------------------------------------------------+
//|                                            timeframe-changer.mq4 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

ENUM_TIMEFRAMES tf[] = {PERIOD_MN1,PERIOD_W1,PERIOD_D1,PERIOD_H4,PERIOD_H1,PERIOD_M30,PERIOD_M15,PERIOD_M5,PERIOD_M1};
string tf_str[] = {"MN1","W1","D1","H4","H1","M30","M15","M5","M1"};


int OnInit()
  {
//--- indicator buffers mapping
   for(int i=0; i< ArraySize(tf); i++){
      color clr = tf[i] == ChartPeriod() ? clrLightGreen : clrLightGray;
      CreateButtonSymbols(0,"tf_" + tf_str[i],(50*i)+50,20,tf_str[i],clr);
   }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---
   if(id==CHARTEVENT_OBJECT_CLICK){
      string obj=sparam;
      for(int i= 0; i < ArraySize(tf); i++){
         if(obj == "tf_" + tf_str[i]){
            ChartSetSymbolPeriod(0,NULL,tf[i]);
         }
      }
     
   }
  }
//+------------------------------------------------------------------+

void OnDeinit(const int reason){
   for(int i=0; i< ArraySize(tf); i++){
      ObjectDelete(0,"tf_" + tf_str[i]);
   }
}

bool CreateButtonSymbols (long chart_ID, string name, int x, int y , string text, color clr)
  {
   ResetLastError();
   if (!ObjectCreate (0,name, OBJ_BUTTON, 0, 0, 0))
     {
      Print (__FUNCTION__, ": Unable to create the button! Error code = ", GetLastError());
      return(false);
     }
     
   ObjectSetString  (chart_ID, name, OBJPROP_TEXT, text);
   ObjectSetInteger (chart_ID, name, OBJPROP_XDISTANCE, x);
   ObjectSetInteger (chart_ID, name, OBJPROP_YDISTANCE, y);
   ObjectSetInteger (chart_ID, name, OBJPROP_XSIZE, 50);
   ObjectSetInteger (chart_ID, name, OBJPROP_YSIZE, 18);
   ObjectSetInteger (chart_ID, name, OBJPROP_CORNER, CORNER_RIGHT_LOWER);
   ObjectSetInteger (chart_ID, name, OBJPROP_FONTSIZE, 8);
   ObjectSetInteger (chart_ID, name, OBJPROP_COLOR, clrBlack);
   ObjectSetInteger (chart_ID, name, OBJPROP_BGCOLOR, clr);
   ObjectSetInteger (chart_ID, name, OBJPROP_BORDER_COLOR, clrBlack);
   ObjectSetInteger (chart_ID, name, OBJPROP_BACK, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_STATE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTABLE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTED, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_HIDDEN, true);
   ObjectSetInteger (chart_ID, name, OBJPROP_ZORDER,0);
   ObjectSetString  (chart_ID, name, OBJPROP_FONT, "Arial");
   return(true);
  }