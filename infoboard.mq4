//+------------------------------------------------------------------+
//|                                                    infoboard.mq4 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   CreateRect(0,"infoboard rectbg",0,0,300,1200,clrWhite, clrRed);
   CreateRect(0,"infoboard rectbg2",0,0,300,150,clrDarkOliveGreen);
   CreateButton(0,"infoboard symbol",0,0,300,40,Symbol(),clrBlack,clrWhite,1, 14);
   
   CreateLabel(0,"infoboard bid",70,50,"Bid",9,clrYellow,"Century Gothic");
   CreateLabel(0,"infoboard ask",170,50,"Ask",9,clrYellow,"Century Gothic");
   CreateLabel(0,"infoboard bidVal",70,65,DoubleToStr(SymbolInfoDouble(Symbol(),SYMBOL_BID),_Digits),12,clrWhite,"Century Gothic");
   CreateLabel(0,"infoboard askVal",170,65,DoubleToStr(SymbolInfoDouble(Symbol(),SYMBOL_ASK),_Digits),12,clrWhite,"Century Gothic");
   
   CreateLabel(0,"infoboard currPrice",150,95,DoubleToStr(SymbolInfoDouble(Symbol(),SYMBOL_LAST),_Digits),16,clrWhite,"Century Gothic",ANCHOR_CENTER,ALIGN_CENTER);
   
   CreateButton(0,"infoboard account",0,130,300,20,"Account Info",clrBlack,clrWhite,1, 10);
   
   CreateLabel(0,"infoboard lblAcc",95,160,"Acc. No:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valAcc",105,160,AccountInfoString(ACCOUNT_NAME),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblCom",95,175,"Broker:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valCom",105,175,AccountInfoString(ACCOUNT_COMPANY),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblCur",95,190,"Currency:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valCur",105,190,AccountInfoString(ACCOUNT_CURRENCY),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblBal",95,205,"Balance:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valbal",105,205,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountBalance(),2),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
  
   CreateLabel(0,"infoboard lblCre",95,220,"Credit:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valCre",105,220,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountCredit(),2),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblEq",95,235,"Equity:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valEq",105,235,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountEquity(),2),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblMargin",95,250,"Margin:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valMargin",105,250,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountMargin(),2),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblFree",95,265,"Free Margin:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valFree",105,265,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountFreeMargin(),2),8,clrBlack,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);
   
   CreateLabel(0,"infoboard lblPL",95,280,"Profit/Loss:",8,clrGray,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard valPL",105,280,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountProfit(),2),10,AccountProfit()>0?clrBlue:clrRed,"Century Gothic Bold",ANCHOR_LEFT,ANCHOR_LEFT);

   CreateRect(0,"infoboard rectItem",0,310,100,1200,clrBlack, clrNONE);
   CreateButton(0,"infoboard positions",0,300,300,20,"Positions",clrBlack,clrWhite,1, 10);
   
   CreateLabel(0,"infoboard lblLong",150,330,"Long",8,clrGray,"Century Gothic Italic",ANCHOR_CENTER,ANCHOR_CENTER);
   CreateLabel(0,"infoboard lblShort",250,330,"Short",8,clrGray,"Century Gothic Italic",ANCHOR_CENTER,ANCHOR_CENTER);
   
   CreateLabel(0,"infoboard lblPos",80,350,"Positions",8,clrWhite,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard lblProfit_",80,370,"Profit",8,clrWhite,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard lblLoss_",80,390,"Loss",8,clrWhite,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   CreateLabel(0,"infoboard lblPL_",80,410,"P/L",8,clrWhite,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT);
   
   CreateLabel(0,"infoboard lblCountBuy",150,350,IntegerToString(CountBuy()),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   CreateLabel(0,"infoboard lblCountSell",250,350,IntegerToString(CountSell()),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   
   CreateLabel(0,"infoboard lblProfitBuy",150,370,DoubleToStr(ProfitBuy(),2),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   CreateLabel(0,"infoboard lblProfitSell",250,370,DoubleToStr(ProfitSell(),2),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   
   CreateLabel(0,"infoboard lblLossBuy",150,390,DoubleToStr(LossBuy(),2),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   CreateLabel(0,"infoboard lblLossSell",250,390,DoubleToStr(LossSell(),2),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   
   CreateLabel(0,"infoboard lblPLBuy",150,410,DoubleToStr(PLBuy(),2),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   CreateLabel(0,"infoboard lblPLSell",250,410,DoubleToStr(PLSell(),2),10,clrBlack,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
   
  
   
//---
   return(INIT_SUCCEEDED);
  }
  
void OnDeinit(const int reason){
   ObjectsDeleteAll(0);
}

void SymbolPositions(string &sym[]){
   string res = "";
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      res+=OrderSymbol() + ",";
   }
   
   AllSymbols(res,sym);
}

int CountBuy(){
   int res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_BUY){
         res++;
      }
   }
   
   return res;
}

int CountSell(){
   int res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_SELL){
         res++;
      }
   }
   
   return res;
}

double PLBuy(){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_BUY){
         res+=OrderProfit();
      }
   }
   
   return res;
}

double PLSell(){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_SELL){
         res+=OrderProfit();
      }
   }
   
   return res;
}


double ProfitBuy(){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_BUY){
         if(OrderProfit()>0)
            res+=OrderProfit();
      }
   }
   
   return res;
}

double ProfitSell(){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_SELL){
         if(OrderProfit()>0)
            res+=OrderProfit();
      }
   }
   
   return res;
}

double LossBuy(){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_BUY){
         if(OrderProfit()<0)
            res+=OrderProfit();
      }
   }
   
   return res;
}

double LossSell(){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_SELL){
         if(OrderProfit()<0)
            res+=OrderProfit();
      }
   }
   
   return res;
}

double PLSymbolBuy(string sym){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_BUY && OrderSymbol()==sym){
            res+=OrderProfit();
      }
   }
   
   return res;
}

double PLSymbolSell(string sym){
   double res = 0;
   for(int i=OrdersTotal()-1; i>=0; i--)           
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES))
      
      if(OrderType() == OP_SELL && OrderSymbol()==sym){
            res+=OrderProfit();
      }
   }
   
   return res;
}



void AllSymbols(string sym, string &result[]){
      string sep=",";                // A separator as a character
      ushort u_sep;              // An array to get strings
      //--- Get the separator code
      u_sep=StringGetCharacter(sep,0);
      //--- Split the string to substrings
      int k=StringSplit(sym,u_sep,result);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   ObjectSetString(0,"infoboard bidVal",OBJPROP_TEXT,DoubleToStr(SymbolInfoDouble(Symbol(),SYMBOL_BID),_Digits));
   ObjectSetString(0,"infoboard askVal",OBJPROP_TEXT,DoubleToStr(SymbolInfoDouble(Symbol(),SYMBOL_ASK),_Digits));
   ObjectSetString(0,"infoboard currPrice",OBJPROP_TEXT,DoubleToStr(close[0],_Digits));
   
   ObjectSetString(0,"infoboard valbal",OBJPROP_TEXT,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountBalance(),2));
   ObjectSetString(0,"infoboard valCre",OBJPROP_TEXT,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountCredit(),2));
   ObjectSetString(0,"infoboard valEq",OBJPROP_TEXT,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountEquity(),2));
   ObjectSetString(0,"infoboard valMargin",OBJPROP_TEXT,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountMargin(),2));
   ObjectSetString(0,"infoboard valFree",OBJPROP_TEXT,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountFreeMargin(),2));
   ObjectSetString(0,"infoboard valPL",OBJPROP_TEXT,AccountInfoString(ACCOUNT_CURRENCY) + DoubleToStr(AccountProfit(),2));
   
   ObjectSetString(0,"infoboard lblCountBuy",OBJPROP_TEXT,IntegerToString(CountBuy()));
   ObjectSetString(0,"infoboard lblCountSell",OBJPROP_TEXT,IntegerToString(CountSell()));
   ObjectSetString(0,"infoboard lblProfitBuy",OBJPROP_TEXT,DoubleToStr(ProfitBuy(),2));
   ObjectSetString(0,"infoboard lblProfitSell",OBJPROP_TEXT,DoubleToStr(ProfitSell(),2));
   ObjectSetString(0,"infoboard lblLossBuy",OBJPROP_TEXT,DoubleToStr(LossBuy(),2));
   ObjectSetString(0,"infoboard lblLossSell",OBJPROP_TEXT,DoubleToStr(LossSell(),2));
   ObjectSetString(0,"infoboard lblPLBuy",OBJPROP_TEXT,DoubleToStr(PLBuy(),2));
   ObjectSetString(0,"infoboard lblPLSell",OBJPROP_TEXT,DoubleToStr(PLSell(),2));
   
   
   ObjectsDeleteAll(0, "infosymbol", 0, OBJ_LABEL);
   string mysymbols[];
   SymbolPositions(mysymbols);
   ArraySort(mysymbols);
   
   int sz = ArraySize(mysymbols);
   
   string s = "";
   for(int i=0; i<sz; i++){
      s+=mysymbols[i] + " | ";
   }
  
   Print("How many counter: " + IntegerToString(sz));
   Print("Counter: " + s);
   //2020.06.30 14:33:17.637	infoboard USDCAD,H1: Counter: OILMn-AUG20 | GOLD | GBPCHF | GBPCHF | EURJPY | EURJPY | EURJPY | EURGBP | EURGBP |  | 

   for(int i=0, n=0; i<sz; i++){
      if(StringLen(mysymbols[i]) > 0){
      //Print("my symbol is: ------------------------------------------------------------->>" + mysymbols[i]);
         
         double plbuy_ = PLSymbolBuy(mysymbols[i]);
         double plsell_ = PLSymbolSell(mysymbols[i]);
         if(CreateLabel(0,"infosymbol " + mysymbols[i],80,(n*20) + 450,mysymbols[i],8,clrWhite,"Century Gothic Italic",ANCHOR_RIGHT,ANCHOR_RIGHT)){
         
         if(plbuy_ != 0)
         CreateLabel(0,"infosymbol long " + mysymbols[i],150,(n*20) + 450,DoubleToStr(plbuy_,2),10,plbuy_>=0 ? clrBlue : clrRed,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
         
         if(plsell_ != 0)
         CreateLabel(0,"infosymbol short " + mysymbols[i],250,(n*20) + 450,DoubleToStr(plsell_,2),10,plsell_>=0 ? clrBlue : clrRed,"Century Gothic Bold",ANCHOR_CENTER,ANCHOR_CENTER);
      
         n++;
         }else{
         
         }
      }
   }
//--- return value of prev_calculated for next call
   return(rates_total);
  }

bool alreadyCreated(string str, string &arr[], int n){
   bool res = false;
   if(n<0){ 
       
      for(int i = 0; i<=n; i++){
         if(arr[i] == str){
            res = true;
            break;
         }
         
      }
      
   }
   return res;
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---
   
  }
//+------------------------------------------------------------------+


bool CreateButton (long chart_ID, string name, int x, int y , int width, int height, string text, 
                   const color clr_bg=clrBlack,const color clr_font=clrWhite, const int align=1, const int font_size=10)
  {
   ResetLastError();
   if (!ObjectCreate (0,name, OBJ_BUTTON, 0, 0, 0))
     {
      Print (__FUNCTION__, ": Unable to create the button! Error code = ", GetLastError());
      return(false);
     }
     
   ObjectSetString  (chart_ID, name, OBJPROP_TEXT, text);
   ObjectSetInteger (chart_ID, name, OBJPROP_XDISTANCE, x);
   ObjectSetInteger (chart_ID, name, OBJPROP_YDISTANCE, y);
   ObjectSetInteger (chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger (chart_ID,name,OBJPROP_YSIZE,height);
   //ObjectSetInteger (chart_ID, name, OBJPROP_CORNER, InpPosition);
   ObjectSetInteger (chart_ID, name, OBJPROP_FONTSIZE, font_size);
   ObjectSetInteger (chart_ID, name, OBJPROP_COLOR, clr_font);
   ObjectSetInteger (chart_ID, name, OBJPROP_BGCOLOR, clr_bg);
   ObjectSetInteger (chart_ID, name, OBJPROP_ALIGN, align);
   ObjectSetInteger (chart_ID, name, OBJPROP_BORDER_COLOR, clrDimGray);
   ObjectSetInteger (chart_ID, name, OBJPROP_BACK, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_STATE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTABLE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTED, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_HIDDEN, true);
   ObjectSetInteger (chart_ID, name, OBJPROP_ZORDER,0);
   ObjectSetString  (chart_ID, name, OBJPROP_FONT, "Arial");
   return(true);
  }
  
  bool CreateRect(long chart_ID, string name, int x, int y, int width, int height, const color clr_bg=clrWhite, const color clr_border=clrBlack){
   ObjectCreate(chart_ID,name,OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,clr_bg);
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_TYPE,BORDER_FLAT);
   ObjectSetInteger(chart_ID, name,OBJPROP_BORDER_COLOR, clr_border);
   //ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,InpPosition);
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clrGray);
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,STYLE_SOLID);
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,3);
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,true);
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,0);
//   
//   if(InpPosition == CORNER_LEFT_UPPER){
//      ObjectSetInteger(0,name,OBJPROP_ANCHOR,ANCHOR_LEFT_UPPER);
//   }else if(InpPosition == CORNER_LEFT_LOWER){
//      ObjectSetInteger(0,name,OBJPROP_ANCHOR,ANCHOR_LEFT_LOWER);
//   }else if(InpPosition == CORNER_RIGHT_UPPER){
//      ObjectSetInteger(0,name,OBJPROP_ANCHOR,ANCHOR_RIGHT_UPPER);
//   }else if(InpPosition == CORNER_RIGHT_LOWER){
//      ObjectSetInteger(0,name,OBJPROP_ANCHOR,ANCHOR_RIGHT_LOWER);
//   }else{
//      ObjectSetInteger(0,name,OBJPROP_ANCHOR,ANCHOR_LEFT_UPPER);
//   }
   
   
   //OBJPROP_ANCHOR 
   
   return true;
  }
  
  
  bool CreateLabel (long chart_ID, string name, int x, int y, string text, int fontSize, 
                    const color clr_font=clrBlack, const string font_family = "Arial Bold",
                    const int anchor = ANCHOR_LEFT, const int align = ALIGN_LEFT)
  {
   ResetLastError();
   if (!ObjectCreate (0,name, OBJ_LABEL, 0, 0, 0))
     {
      Print (__FUNCTION__, ": Unable to create the label! Error code = ", GetLastError());
      return(false);
     }
     
   ObjectSetString  (chart_ID, name, OBJPROP_TEXT, text);
   ObjectSetInteger (chart_ID, name, OBJPROP_XDISTANCE, x);
   ObjectSetInteger (chart_ID, name, OBJPROP_YDISTANCE, y);
   ObjectSetInteger (chart_ID, name, OBJPROP_ANCHOR,anchor);//ANCHOR_CENTER
   ObjectSetInteger (chart_ID, name, OBJPROP_ALIGN,align);//ANCHOR_CENTER
   ObjectSetInteger (chart_ID, name, OBJPROP_FONTSIZE, fontSize);
   ObjectSetInteger (chart_ID, name, OBJPROP_COLOR, clr_font);
   ObjectSetInteger (chart_ID, name, OBJPROP_BGCOLOR, clrGray);
   ObjectSetInteger (chart_ID, name, OBJPROP_BACK, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_STATE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTABLE, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_SELECTED, false);
   ObjectSetInteger (chart_ID, name, OBJPROP_HIDDEN, true);
   ObjectSetInteger (chart_ID, name, OBJPROP_ZORDER,0);
   ObjectSetString  (chart_ID, name, OBJPROP_FONT, font_family);
   return(true);
  }
  
//+------------------------------------------------------------------+ 
//| Create Edit object                                               | 
//+------------------------------------------------------------------+ 
bool EditCreate(const long             chart_ID=0,               // chart's ID 
                const string           name="Edit",              // object name 
                const int              sub_window=0,             // subwindow index 
                const int              x=0,                      // X coordinate 
                const int              y=0,                      // Y coordinate 
                const int              width=50,                 // width 
                const int              height=18,                // height 
                const string           text="Text",              // text 
                const string           font="Arial",             // font 
                const int              font_size=10,             // font size 
                const ENUM_ALIGN_MODE  align=ALIGN_CENTER,       // alignment type 
                const bool             read_only=false,          // ability to edit 
                const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // chart corner for anchoring 
                const color            clr=clrBlack,             // text color 
                const color            back_clr=clrWhite,        // background color 
                const color            border_clr=clrNONE,       // border color 
                const bool             back=false,               // in the background 
                const bool             selection=false,          // highlight to move 
                const bool             hidden=true,              // hidden in the object list 
                const long             z_order=0)                // priority for mouse click 
  { 
//--- reset the error value 
   ResetLastError(); 
//--- create edit field 
   if(!ObjectCreate(chart_ID,name,OBJ_EDIT,sub_window,0,0)) 
     { 
      Print(__FUNCTION__, 
            ": failed to create \"Edit\" object! Error code = ",GetLastError()); 
      return(false); 
     } 
//--- set object coordinates 
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x); 
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y); 
//--- set object size 
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width); 
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height); 
//--- set the text 
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text); 
//--- set text font 
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font); 
//--- set font size 
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size); 
//--- set the type of text alignment in the object 
   ObjectSetInteger(chart_ID,name,OBJPROP_ALIGN,align); 
//--- enable (true) or cancel (false) read-only mode 
   ObjectSetInteger(chart_ID,name,OBJPROP_READONLY,read_only); 
//--- set the chart's corner, relative to which object coordinates are defined 
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner); 
//--- set text color 
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr); 
//--- set background color 
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr); 
//--- set border color 
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr); 
//--- display in the foreground (false) or background (true) 
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back); 
//--- enable (true) or disable (false) the mode of moving the label by mouse 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection); 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection); 
//--- hide (true) or display (false) graphical object name in the object list 
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden); 
//--- set the priority for receiving the event of a mouse click in the chart 
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order); 
//--- successful execution 
   return(true); 
  } 